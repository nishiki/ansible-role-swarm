# Ansible role: Docker Swarm

[![Version](https://img.shields.io/badge/latest_version-1.1.0-green.svg)](https://code.waks.be/nishiki/ansible-role-swarm/releases)
[![License](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://code.waks.be/nishiki/ansible-role-swarm/src/branch/main/LICENSE)
[![Build](https://code.waks.be/nishiki/ansible-role-swarm/actions/workflows/molecule.yml/badge.svg?branch=main)](https://code.waks.be/nishiki/ansible-role-swarm/actions?workflow=molecule.yml)

Install and configure docker with swarm

## Requirements

- Ansible >= 2.10
- Debian Bookworm

## Role variables

- `swarm_init` - init the cluster, run once with the ansible option `-e swarm_init=true` (default: `false`)
- `swarm_manager` - set the cluster role (default: `false`)
- `swarm_advertise_addr` - listen address
- `swarm_join_token_manager` - join token for manager (use `docker swarm join-token manager` after init to set this variable)
- `swarm_join_token_worker` - join token for worker (use `docker swarm join-token worker` after init to set this variable)
- `swarm_remote_addrs` - manager addresses for slave node
- `swarm_networks` - array with the docker networks

```
proxy:
  state: present
```

- `swarm_services` - hash with the service to manage

```
wordpress:
  image: wordpress:latest
  args:
    - '--api'
  mounts:
    - source: /opt/data
      target: /usr/local/data
      type: bind
  networks:
    - net
  publish:
    - published_port: 80
      target_port: 8000
  replicas: 3
  limits:
    cpus: 0.5
    memory: 100M
  env:
    WORDPRESS_DB_HOST: db:3306
    WORDPRESS_DB_USER: wordpress
    WORDPRESS_DB_PASSWORD: wordpress
  container_labels:
    region: FR
  init: false
  state: present
```

- `swarm_registry_logins` - hash with the registry logins

```
gitlab:
  registry_url: registry.gitlab.com
  registry_username: myaccount
  registry_password: secret
  user: www-data
```

- `swarm_proxy_url` - set a proxy url for http and https requests
- `swarm_proxy_ignore` - array with ignore host or subnet

```
  - localhost
  - 10.0.0.0/8
```

## How to use

- Install:

```
- hosts: server
  roles:
    - docker
```

## Development

### Test with molecule and docker

- install [docker](https://docs.docker.com/engine/installation/)
- install `python3` and `python3-pip`
- install molecule and dependencies `pip3 install molecule 'molecule[docker]' docker ansible-lint testinfra yamllint`
- run `molecule test`

## License

```
Copyright (c) 2018 Adrien Waksberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
