import testinfra.utils.ansible_runner
import time

time.sleep(60)

def test_packages(host):
  package = host.package('docker.io')
  assert package.is_installed

def test_default_config_file(host):
  path = host.file('/etc/default/docker')
  assert path.exists
  assert path.is_file
  assert path.user == 'root'
  assert path.group == 'root'
  assert path.mode == 0o644

def test_service(host):
  service = host.service('docker')
  assert service.is_running
  assert service.is_enabled

def test_sockets(host):
  socket = host.socket('tcp://0.0.0.0:80')
  assert socket.is_listening

def test_docker_network(host):
  cmd = host.run('docker network list')
  assert cmd.succeeded
  assert 'proxy' in cmd.stdout

def test_docker_container(host):
  cmd = host.run('docker ps')
  assert cmd.succeeded
  assert 'whoami' in cmd.stdout
  assert 'traefik' in cmd.stdout

def test_databases(host):
  cmd = host.run('curl -H "Host: whoami" http://127.0.0.1')
  assert cmd.succeeded
  assert "I'm " in cmd.stdout
